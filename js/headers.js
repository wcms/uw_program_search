/**
 * @file
 * Persistant Headers on Result Page.
 */

function setHeaderPersist() {
  jQuery('.results-title').each(function () {
    if (jQuery(window).scrollTop() < jQuery(this).parent().offset().top) {
      jQuery(this).removeClass('fixed-header');
      jQuery(this).removeAttr('style');
      jQuery(this).siblings('.results').removeAttr('style');
    }
    else if (jQuery(window).scrollTop() >= jQuery(this).offset().top) {
      jQuery(this).addClass('fixed-header');
      jQuery(this).width(jQuery(this).parent('.column').width());
      var header_height = jQuery(this).outerHeight();
      jQuery(this).siblings('.results').css("margin-top", header_height + 'px');
    }
  });
}
jQuery(window).scroll(function () {
  setHeaderPersist();
});
jQuery(window).resize(function () {
  setHeaderPersist();
});
